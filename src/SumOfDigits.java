import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class SumOfDigits {
    public static void main(String[] args) throws Exception {

        int num = 0;
        int score = 0;
        Map map = new HashMap<Integer, Integer>();
        
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a String :");
        
        String lineOfNumbers = scan.nextLine();
        System.out.println("input string words separated by whitespace: " + lineOfNumbers);        
        
        String[] numberStrs = lineOfNumbers.split(" ");
        System.out.println("input string after split: " + Arrays.toString(numberStrs));
        
        for(int i = 0;i < numberStrs.length;i++)
        {
            num = Integer.parseInt(numberStrs[i]);

            score = sumOfDigits(num);

            map.put(num, score);

            num =0;
            score=0;
        }
        System.out.println("output string: " + map.toString());
    }

    public static int sumOfDigits(int num) 
        {
            int sum = 0;

            while (num > 0)
            {
                sum = sum + num % 10;
                num = num / 10;
            }

            sum = (sum <10) ? sum : sumOfDigits(sum);

            return sum;


        }
    
}
